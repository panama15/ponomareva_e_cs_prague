
# (определение функций)
import simple_draw as sd
import random
sd.resolution = (1200, 600)


# Создать функцию которая нарисует шахматную доску по двум параметрам.

# Функцию следующего вида      def desk(yy, xx):
# Где уу = высота доски(количество квадратов по вертикали), хх = ширина доски(количесво квадратов по горизонтали)
def desk1(yy,xx):
    sd.background_color=sd.COLOR_BLACK
    for y in range(1, (100*yy), 200):
         start_point=sd.get_point(1,y)
         for x in range(1, (100*xx), 200):
            start_point=sd.get_point(x,y)
            sd.square(start_point,100,sd.COLOR_WHITE,0)
    for y in range(101, (100*yy), 200):
         start_point1=sd.get_point(101,y)
         for x in range(101, (100*xx), 200):
            start_point1=sd.get_point(x,y)
            sd.square(start_point1,100,sd.COLOR_WHITE,0)

desk1(3,5)


sd.pause()