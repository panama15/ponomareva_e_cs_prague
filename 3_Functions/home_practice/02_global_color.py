# -*- coding: utf-8 -*-
import simple_draw as sd
sd.background_color = sd.COLOR_BLACK
sd.resolution = (1200, 800)
# Добавить цвет в функции рисования геом. фигур. из упр 01_shapes.py
# (код функций скопировать сюда и изменить)
# Запросить у пользователя цвет фигуры посредством выбора из существующих:
#   вывести список всех цветов с номерами и ждать ввода номера желаемого цвета.
# Потом нарисовать все фигуры этим цветом

# Пригодятся функции
# sd.get_point()
# sd.line()
# sd.get_vector()
# и константы COLOR_RED, COLOR_ORANGE, COLOR_YELLOW, COLOR_GREEN, COLOR_CYAN, COLOR_BLUE, COLOR_PURPLE
# Результат решения см results/exercise_02_global_color.jpg

colours= [sd.COLOR_RED, sd.COLOR_ORANGE,sd.COLOR_YELLOW,
         sd.COLOR_GREEN,sd.COLOR_CYAN,sd.COLOR_BLUE,sd.COLOR_PURPLE]


while True:
    vvod = int(input('Введите желаемый цвет', ))

    if vvod<7:
     colour = colours[vvod]
     break
    else:
     print("neverno")


def triangle():
 point=sd.get_point(200,200)
 v1=sd.get_vector(point,0,200,4)
 v1.draw(colour)
 v2=sd.get_vector(v1.end_point,120,200,4)
 v2.draw(colour)
 v3=sd.get_vector(v2.end_point,240,200,4)
 v3.draw(colour)

def square():
 point1=sd.get_point(500,200)
 v4=sd.get_vector(point1,0,100,4)
 v4.draw(colour)

 v5=sd.get_vector(v4.end_point,90,100,4)
 v5.draw(colour)
 v6=sd.get_vector(v5.end_point,180,100,4)
 v6.draw(colour)
 v7=sd.get_vector(v6.end_point,270,100,4)
 v7.draw(colour)


def petiug():
 point2=sd.get_point(850,200)
 v8=sd.get_vector(point2,0,100,4)
 v8.draw(colour)
 v9=sd.get_vector(v8.end_point,72,100,4)
 v9.draw(colour)
 v10=sd.get_vector(v9.end_point,144,100,4)
 v10.draw(colour)
 v11=sd.get_vector(v10.end_point,216,100,4)
 v11.draw(colour)
 v12=sd.get_vector(v8.start_point,108,100,4)
 v12.draw(colour)

def shestiug():
 point3=sd.get_point(850,500)
 v13=sd.get_vector(point3,0,100,4)
 v13.draw(colour)
 v14=sd.get_vector(v13.end_point,60,100,4)
 v14.draw(colour)
 v15=sd.get_vector(v14.end_point,120,100,4)
 v15.draw(colour)
 v16=sd.get_vector(v15.end_point,180,100,4)
 v16.draw(colour)
 v17=sd.get_vector(v16.end_point,240,100,4)
 v17.draw(colour)
 v18=sd.get_vector(v13.start_point,120,100,4)
 v18.draw(colour)

triangle()
sd.pause()