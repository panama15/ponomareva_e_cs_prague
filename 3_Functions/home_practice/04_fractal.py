# -*- coding: utf-8 -*-


# 1) Написать функцию draw_branches, которая должна рисовать две ветви дерева из начальной точки
# Функция должна принимать параметры:
# - точка начала рисования,
# - угол рисования,
# - длина ветвей,
# Отклонение ветвей от угла рисования принять 30 градусов,

# 2) Сделать draw_branches рекурсивной
# - добавить проверку на длину ветвей, если длина меньше 10 - не рисовать
# - вызывать саму себя 2 раза из точек-концов нарисованных ветвей,
#   с параметром "угол рисования" равным углу только что нарисованной ветви,
#   и параметром "длинна ветвей" в 0.75 меньшей чем длина только что нарисованной ветви

# 3) Запустить вашу рекурсивную функцию, используя следующие параметры:
# root_point = sd.get_point(300, 30)
# draw_branches(start_point=root_point, angle=90, length=100)

# Пригодятся функции
# sd.get_point()
# sd.get_vector()
# Возможный результат решения см results/exercise_04_fractal_01.jpg

# можно поиграть -шрифтами- цветами и углами отклонения



# 4) Усложненное задание (делать по желанию)
# - сделать рандомное отклонение угла ветвей в пределах 40% от 30-ти градусов
# - сделать рандомное отклонение длины ветвей в пределах 20% от коэффициента 0.75
# Возможный результат решения см results/exercise_04_fractal_02.jpg

# Пригодятся функции
# sd.random_number()

import simple_draw as sd
sd.resolution=(1000,600)

def branch(point, angle, length, delta):
    if length < 10:
        return
    v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=3)
    v1.draw()
    next_point = v1.end_point
    next_angle = angle - delta
    next_angle_1 = angle + delta
    next_length = length * .75
    # sd.sleep(0.1)
    branch(point=next_point, angle=next_angle, length=next_length, delta=delta)
    branch(point=next_point, angle=next_angle_1, length=next_length, delta=delta)

def branch1(point, angle, length, delta):
        a = sd.random_number(50, 90)
        if length < 4:
            return
        v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=3)
        v1.draw()
        next_point = v1.end_point
        next_angle = angle - delta
        next_angle_1 = angle +delta
        next_length = length *(a/100)
        delta=sd.random_number(15,40)
        # sd.sleep(0.1)
        branch(point=next_point, angle=next_angle, length=next_length, delta=delta)
        branch(point=next_point, angle=next_angle_1, length=next_length, delta=delta)





point_0 = sd.get_point(300, 30)
# branch(point_0, angle=90, length=100, delta=30)
# branch1(point_0,angle=90,length=100,delta=sd.random_number(18,42))



def lepstok(point, angle, length,delta):
        if length < 10:
            return
        v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=3)
        v1.draw()
        next_point = v1.end_point
        next_angle = angle-delta
        next_length = length * .75
        # sd.sleep(0.1)
        lepstok(point=next_point, angle=next_angle, length=next_length, delta=delta)

for y in range(0,200,90):
    point_1=sd.get_point(300,y)
    lepstok(point_1,angle=90,length=100,delta=30)
    lepstok(point_1,angle=90,length=100,delta=330)




sd.pause()