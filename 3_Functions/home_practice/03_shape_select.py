# -*- coding: utf-8 -*-

# Запросить у пользователя желаемую фигуру посредством выбора из существующих
#   вывести список всех фигур с номерами и ждать ввода номера желаемой фигуры.
# и нарисовать эту фигуру в центре экрана

# Код функций из упр 02_global_color.py скопировать сюда
# Результат решения см results/exercise_03_shape_select.jpg

import  simple_draw as sd
sd.resolution=(1000,600)


def triangle():
 point=sd.get_point(200,200)
 v1=sd.get_vector(point,0,200,4)
 v1.draw()
 v2=sd.get_vector(v1.end_point,120,200,4)
 v2.draw()
 v3=sd.get_vector(v2.end_point,240,200,4)
 v3.draw()

def square():
 point1=sd.get_point(500,200)
 v4=sd.get_vector(point1,0,100,4)
 v4.draw()
 v5=sd.get_vector(v4.end_point,90,100,4)
 v5.draw()
 v6=sd.get_vector(v5.end_point,180,100,4)
 v6.draw()
 v7=sd.get_vector(v6.end_point,270,100,4)
 v7.draw()


def petiug():
 point2=sd.get_point(850,200)
 v8=sd.get_vector(point2,0,100,4)
 v8.draw()
 v9=sd.get_vector(v8.end_point,72,100,4)
 v9.draw()
 v10=sd.get_vector(v9.end_point,144,100,4)
 v10.draw()
 v11=sd.get_vector(v10.end_point,216,100,4)
 v11.draw()
 v12=sd.get_vector(v8.start_point,108,100,4)
 v12.draw()

def shestiug():
 point3=sd.get_point(850,500)
 v13=sd.get_vector(point3,0,100,4)
 v13.draw()
 v14=sd.get_vector(v13.end_point,60,100,4)
 v14.draw()
 v15=sd.get_vector(v14.end_point,120,100,4)
 v15.draw()
 v16=sd.get_vector(v15.end_point,180,100,4)
 v16.draw()
 v17=sd.get_vector(v16.end_point,240,100,4)
 v17.draw()
 v18=sd.get_vector(v13.start_point,120,100,4)
 v18.draw()

zapros=int(input("insert number of the figure"))

if zapros==0:
     triangle()
elif zapros==1:
    square()
elif zapros==2:
    petiug()
else:
    shestiug()

sd.pause()