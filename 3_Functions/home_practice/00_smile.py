# (определение функций)
import simple_draw as sd
import random
sd.resolution = (1200, 900)


# Написать функцию отрисовки смайлика по заданным координатам
# Форма рожицы-смайлика на ваше усмотрение
# Параметры функции: кордината X, координата Y, цвет.
# Вывести 10 смайликов в произвольных точках экрана.

for y in range(50,901,140):
     for x in range(100,1000,500):
         point = sd.get_point(x, y)
         point1 = sd.get_point((x-20), y)
         point2 = sd.get_point((x+20), y)
         point3 = sd.get_point((x-20), (y - 30))
         point4 = sd.get_point((x+20), (y - 30))
         radius = 70
         radius1 = 10
         sd.circle(point, radius, sd.COLOR_YELLOW, 6)
         sd.circle(point1, radius1, sd.COLOR_YELLOW, 0)
         sd.circle(point2, radius1, sd.COLOR_YELLOW, 0)
         sd.rectangle(point3, point4, sd.COLOR_YELLOW, 6)



sd.pause()