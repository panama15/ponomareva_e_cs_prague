# -*- coding: utf-8 -*-

import simple_draw as sd

sd.resolution = (1200, 900)


# Часть 1.
# Написать функции рисования равносторонних геометрических фигур:
# - треугольника
# - квадрата
# - пятиугольника
# - шестиугольника
# Все функции должны принимать 3 параметра:
# - точка начала рисования
# - угол наклона
# - длина стороны
#
# Примерный алгоритм внутри функции:
#   # будем рисовать с помощью векторов, каждый следующий - из конечной точки предыдущего
#   текущая_точка = начальная точка
#   для угол_наклона из диапазона от 0 до 360 с шагом XXX
#      # XXX подбирается индивидуально для каждой фигуры
#      составляем вектор из текущая_точка заданной длины с наклоном в угол_наклона
#      рисуем вектор
#      текущая_точка = конечной точке вектора
#
# Использование копи-пасты - обязательно! Даже тем кто уже знает про её пагубность. Для тренировки.
# Как работает копипаста:
#   - одну функцию написали,
#   - копипастим её, меняем название, чуть подправляем код,
#   - копипастим её, меняем название, чуть подправляем код,
#   - и так далее.
# В итоге должен получиться ПОЧТИ одинаковый код в каждой функции

# Пригодятся функции
# sd.get_point()
# sd.get_vector()
# sd.line()
# Результат решения см /results/exercise_01_shapes.jpg


# Часть 1-бис.
# Попробуйте прикинуть обьем работы, если нужно будет внести изменения в этот код.
# Скажем, связывать точки не линиями, а дугами. Или двойными линиями. Или рисовать круги в угловых точках. Или...
# А если таких функций не 4, а 44? Код писать не нужно, просто представь объем работы... и запомни это.
# зачет

# Часть 2 (делается после зачета первой части)
#
# Надо сформировать функцию, параметризированную в местах где была "небольшая правка".
# Это называется "Выделить общую часть алгоритма в отдельную функцию"
# Потом надо изменить функции рисования конкретных фигур - вызывать общую функцию вместо "почти" одинакового кода.
#
# В итоге должно получиться:
#   - одна общая функция со множеством параметров,
#   - все функции отрисовки треугольника/квадрата/етс берут 3 параметра и внутри себя ВЫЗЫВАЮТ общую функцию.
#
# Не забудте в этой общей функции придумать, как устранить разрыв в начальной/конечной точках рисуемой фигуры
# (если он есть. подсказка - на последней итерации можно использовать линию от первой точки)

# Часть 2-бис.
# А теперь - сколько надо работы что бы внести изменения в код? Выгода на лицо :)
# Поэтому среди программистов есть принцип D.R.Y. https://clck.ru/GEsA9
# Будьте ленивыми, не используйте копи-пасту!
import simple_draw as sd
sd.background_color = sd.COLOR_BLACK
sd.resolution = (1200, 900)

point=sd.get_point(200,200)
v1=sd.get_vector(point,0,200,4)
v1.draw()
v2=sd.get_vector(v1.end_point,120,200,4)
v2.draw()
v3=sd.get_vector(v2.end_point,240,200,4)
v3.draw()

point1=sd.get_point(500,200)
v4=sd.get_vector(point1,0,100,4)
v4.draw()
v5=sd.get_vector(v4.end_point,90,100,4)
v5.draw()
v6=sd.get_vector(v5.end_point,180,100,4)
v6.draw()
v7=sd.get_vector(v6.end_point,270,100,4)
v7.draw()

point2=sd.get_point(850,200)
v8=sd.get_vector(point2,0,100,4)
v8.draw()
v9=sd.get_vector(v8.end_point,72,100,4)
v9.draw()
v10=sd.get_vector(v9.end_point,144,100,4)
v10.draw()
v11=sd.get_vector(v10.end_point,216,100,4)
v11.draw()
v12=sd.get_vector(v8.start_point,108,100,4)
v12.draw()

point3=sd.get_point(850,500)
v13=sd.get_vector(point3,0,100,4)
v13.draw()
v14=sd.get_vector(v13.end_point,60,100,4)
v14.draw()
v15=sd.get_vector(v14.end_point,120,100,4)
v15.draw()
v16=sd.get_vector(v15.end_point,180,100,4)
v16.draw()
v17=sd.get_vector(v16.end_point,240,100,4)
v17.draw()
v18=sd.get_vector(v13.start_point,120,100,4)
v18.draw()


def triangle():
   angle=0
   length=100
   width=4
   startpoint = sd.get_point(int(input()), int(input()))
   while angle<181:
     vektor=sd.get_vector(startpoint,angle,length,width)
     startpoint=vektor.end_point
     angle=+60
     vektor.draw()

triangle()




def triangle():
 point=sd.get_point(200,200)
 v1=sd.get_vector(point,0,200,4)
 v1.draw(colour)
 v2=sd.get_vector(v1.end_point,120,200,4)
 v2.draw(colour)
 v3=sd.get_vector(v2.end_point,240,200,4)
 v3.draw(colour)

def square():
 point1=sd.get_point(500,200)
 v4=sd.get_vector(point1,0,100,4)
 v4.draw(colour)
 v5=sd.get_vector(v4.end_point,90,100,4)
 v5.draw(colour)
 v6=sd.get_vector(v5.end_point,180,100,4)
 v6.draw(colour)
 v7=sd.get_vector(v6.end_point,270,100,4)
 v7.draw(colour)


def petiug():
 point2=sd.get_point(850,200)
 v8=sd.get_vector(point2,0,100,4)
 v8.draw(colour)
 v9=sd.get_vector(v8.end_point,72,100,4)
 v9.draw(colour)
 v10=sd.get_vector(v9.end_point,144,100,4)
 v10.draw(colour)
 v11=sd.get_vector(v10.end_point,216,100,4)
 v11.draw(colour)
 v12=sd.get_vector(v8.start_point,108,100,4)
 v12.draw(colour)

def shestiug():
 point3=sd.get_point(850,500)
 v13=sd.get_vector(point3,0,100,4)
 v13.draw(colour)
 v14=sd.get_vector(v13.end_point,60,100,4)
 v14.draw(colour)
 v15=sd.get_vector(v14.end_point,120,100,4)
 v15.draw(colour)
 v16=sd.get_vector(v15.end_point,180,100,4)
 v16.draw(colour)
 v17=sd.get_vector(v16.end_point,240,100,4)
 v17.draw(colour)
 v18=sd.get_vector(v13.start_point,120,100,4)
 v18.draw(colour)






sd.background_color = sd.COLOR_BLACK
sd.pause()

