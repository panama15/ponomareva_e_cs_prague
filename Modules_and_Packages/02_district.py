# -*- coding: utf-8 -*-

# Составить список всех живущих на районе и Вывести на консоль через запятую
# Формат вывода: На районе живут ...
# подсказка: для вывода элементов списка через запятую можно использовать функцию строки .join()
# https://docs.python.org/3/library/stdtypes.html#str.join

from district.central_street.house1 import room1 as r1,room2 as r2
from district.central_street.house2 import room1 as r3,room2 as r4
from district.soviet_street.house1 import room1 as r5,room2 as r6
from district.soviet_street.house2 import room1 as r7,room2 as r8
all=','.join(r1.folks)
all1=','.join(r2.folks)
all2=','.join(r3.folks)
all3=','.join(r4.folks)
all4=','.join(r5.folks)
all5=','.join(r6.folks)
all6=','.join(r7.folks)
all7=','.join(r8.folks)



#print('На районе живут',all,all1,all2,all3,all4,all5,all6,all7)
print('На районе живут',all)
print('На районе живут',all1)
print('На районе живут',all2)
print('На районе живут',all3)
print('На районе живут',all4)
print('На районе живут',all5)
print('На районе живут',all6)
print('На районе живут',all7)