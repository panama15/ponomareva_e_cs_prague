# -*- coding: utf-8 -*-

# Вывести на консоль жителей комнат (модули room_1 и room_2)
# Формат: В комнате room_1 живут: ...




from Modules_and_Packages import room_1 as r1
from Modules_and_Packages import room_2 as r2
print('В комнате 1 живут',r1.folks,',','В комнате 2 живут',r2.folks)
