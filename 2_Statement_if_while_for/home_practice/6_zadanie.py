# (цикл for)
import simple_draw as sd
sd.resolution = (600, 600)
# Нарисовать стену из кирпичей. Размер кирпича - 100х50
# Использовать вложенные циклы for


# Подсказки:
#  Для отрисовки кирпича использовать функцию rectangle
#  Алгоритм должен получиться приблизительно такой:
#
#   цикл по координате Y
#       вычисляем сдвиг ряда кирпичей
#       цикл координате X
#           вычисляем правый нижний и левый верхний углы кирпича
#           рисуем кирпич

for y in range(1,601,50):
    point = sd.get_point(1,y)
    rtop=sd.get_point(101,y)
    for x in range(1,601,100):
        point=sd.get_point(x,y)
        rtop=sd.get_point(x+100,y+50)
        sd.rectangle(point,rtop,sd.COLOR_WHITE,2)

sd.pause()
